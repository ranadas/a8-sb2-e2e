### Spring Boot 2.x + Angular 8 
The REST APIs will have a token-based authentication integrated with Spring Security. At the client side, Angular routes will be protected and we will have Angular interceptors to add the token in every HTTP request.


https://www.devglan.com/spring-boot/spring-boot-angular-8-example

https://github.com/only2dhir/angular8-demo

https://github.com/only2dhir/spring-boot-jwt