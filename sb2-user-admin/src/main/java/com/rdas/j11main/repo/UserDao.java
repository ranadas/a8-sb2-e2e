package com.rdas.j11main.repo;

import com.rdas.j11main.model.*;
import org.springframework.data.repository.CrudRepository;


public interface UserDao extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}